# coding=utf-8
"""
create by fantasylin 2018/11/27
"""
from decimal import Decimal
import gdal
import numpy as np
# from osgeo import osr

class GTifWriter(object):
    """
    GEOTiff文件写入类
    """
    def __init__(self, res, box, imgdata, proj):
        # res = Decimal(str(res[0]))
        #Decimal(str(res / Decimal(100000.0)))if res > 1 else
        # pylint: disable=too-many-instance-attributes
        # Eight is reasonable in this case.
        self.__res__ = Decimal(str(res))
        self.__box__ = [Decimal(str(box[0])), Decimal(str(box[1]))]
        # self.__top_lat__ = Decimal(str(box[0]))
        # self.__top_lon__ = Decimal(str(box[1]))
        self.__img_data__ = imgdata
        self.__proj__ = proj
        if len(imgdata.shape) == 3:
            self.height, self.width, self.band = imgdata.shape
        elif len(imgdata.shape) == 2:
            (self.height, self.width), self.band = imgdata.shape, 1
            self.__img_data__ = self.__img_data__.reshape((self.height, self.width, 1))
        else:
            raise Exception
        return

    def write(self, filename, datatype='INT16'):
        """写文件
        Args:
            datatype: INT16 INT32 FLOAT32 FLOAT64
        """
        datatype = self.__img_data__.dtype.name.upper()
        if datatype == 'INT16':
            datatype = gdal.GDT_Int16
            numpytype = np.int16
        elif datatype == 'INT32':
            datatype = gdal.GDT_Int32
            numpytype = np.int32
        elif datatype == 'FLOAT32':
            datatype = gdal.GDT_Float32
            numpytype = np.float32
        elif datatype == 'FLOAT64':
            datatype = gdal.GDT_Float64
            numpytype = np.float64
        driver = gdal.GetDriverByName('GTiff')
        dataset = driver.Create(filename, self.width,
                                self.height, self.band, datatype)
        dataset.SetGeoTransform(
            [self.__box__[1], self.__res__, 0, self.__box__[0], 0, -1*self.__res__])
        # srs = osr.SpatialReference()
        # srs.SetProjCS('Latlon')
        # # srs.SetProjection('Latlon')
        # srs.SetWellKnownGeogCS('WGS84')
        dataset.SetProjection(self.__proj__.ExportToWkt())
        for i in range(self.band):
            tmpdata = self.__img_data__[:, :, i].astype(numpytype)
            dataset.GetRasterBand(i + 1).WriteArray(tmpdata)
        del dataset
