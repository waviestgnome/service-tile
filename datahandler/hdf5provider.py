# coding=utf-8
"""
Create by fantasylin 2018/11/27
"""

from decimal import Decimal
import h5py
import numpy as np
from osgeo import osr
# from customerr.cus_error import BandError

FILLVALUE = 32767

class HDF5Provider(object):
    """
    HDF5文件读取类
    """
    def __init__(self, filename):
        self.hdf5_obj = h5py.File(filename, 'r')
        self.attrs = self.hdf5_obj.attrs
        self.dataset = []

    def get_dataset(self, dataset_name):
        """获取数据集
        Args:
            datasetName 数据集名
        """
        try:
            self.dataset = np.array(self.hdf5_obj[dataset_name])
            normalwidth = int(round((self.box[1] - self.box[3]) / self.precision + Decimal(0.5)))
            normalheight = int(round((self.box[0] - self.box[2]) / self.precision + Decimal(0.5)))
            if self.dataset.shape != (normalheight, normalwidth):
                normaldata = np.empty((normalheight, normalwidth), dtype=self.dataset.dtype)
                normaldata[:, -1] = FILLVALUE
                normaldata[-1, :] = FILLVALUE
                normaldata[:-1, :-1] = self.dataset
                self.dataset = normaldata
        except KeyError as key_err:
            raise BandError(key_err.message)
        else:
            return self.dataset

    def get_atrribute(self, group, dataset, atrname):
        """
        获取group下dataset的atrname属性
        """
        return str(self.hdf5_obj[group][dataset].attrs[atrname][0])

    @property
    def box(self):
        """
        经纬度范围
        """
        # max_lat = np.array(self.attrs['Maximum Latitude'])[0]
        # max_lon = np.array(self.attrs['Maximum Longitude'])[0]
        # min_lat = np.array(self.attrs['Minimum Latitude'])[0]
        # min_lon = np.array(self.attrs['Minimum Longitude'])[0]
        # print type(self.attrs['Projection Type'])
        # if not isinstance(self.attrs['Projection Type'], np.ndarray):
        #     if self.attrs['Projection Type'].upper() != 'LATLON':
        #         max_lat = np.array(self.attrs['Maximum Latitude'])
        #         max_lon = np.array(self.attrs['Maximum Longitude'])
        #         min_lat = np.array(self.attrs['Minimum Latitude'])
        #         min_lon = np.array(self.attrs['Minimum Longitude'])

        if not isinstance(self.attrs['Maximum Y'], np.ndarray):
            max_lat = np.array(self.attrs['Maximum Y'])
            max_lon = np.array(self.attrs['Maximum X'])
            min_lat = np.array(self.attrs['Minimum Y'])
            min_lon = np.array(self.attrs['Minimum X'])
        else:
            max_lat = np.array(self.attrs['Maximum Y'])[0]
            max_lon = np.array(self.attrs['Maximum X'])[0]
            min_lat = np.array(self.attrs['Minimum Y'])[0]
            min_lon = np.array(self.attrs['Minimum X'])[0]
        return [Decimal(str(max_lat)), Decimal(str(max_lon)), Decimal(str(min_lat)), Decimal(str(min_lon))]

    @property
    def precision(self):
        """
        分辨率
        """
        if not isinstance(self.attrs['Latitude Precision'], np.ndarray):
            lat_precision = np.array(self.attrs['Latitude Precision'])
        else:
            lat_precision = np.array(self.attrs['Latitude Precision'])[0]
        # lon_precision = np.array(self.attrs['Longitude Precision'])[0]
        #Decimal(str(lat_precision / 100000.0)) if lat_precision > 1 else
        lat_precision = Decimal(str(lat_precision))
        # lon_precision = Decimal(str(lon_precision))
        # return [lat_precision, lon_precision]
        return lat_precision

    @property
    def imgsize(self):
        """图像尺寸"""
        height, width = self.dataset.shape
        return [width, height]

    @property
    def projection(self):
        """投影方式"""
        proj = np.array(self.attrs['Projection Type'])
        srs = osr.SpatialReference()
        if str(proj).upper() != 'LATLON':
            srs.SetProjection(str(proj))
        srs.SetWellKnownGeogCS('WGS84')
        # srs.SetProjCS(str(proj))
        return srs
