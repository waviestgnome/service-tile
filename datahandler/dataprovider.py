# coding=utf-8
"""
create by fantasylin 2018/11/27
"""
from os import path
from datahandler.hdf5provider import HDF5Provider
from datahandler.gtifprovider import GTIFProvider
from datahandler.ncprovider import NCProvider
class DataProvider(object):
    """
    数据工厂类
    """
    def __init__(self, filename):
        """init
        Args:
            - filename:
        """
        extension = path.splitext(filename)[1]
        if extension.lower() == '.hdf':
            self.provider = HDF5Provider(filename)
        elif extension.lower() == '.tif':
            self.provider = GTIFProvider(filename)
        elif extension.lower() == '.nc':
            self.provider = NCProvider(filename)
        else:
            self.provider = None

    def get_dataset(self, dataset='0'):
        """
        获取数据集
        """
        return self.provider.get_dataset(dataset)

    def compara(self, other):
        """
        比较两个数据是否一致(box， res)
        """
        # src_res = round(self.precision[0] * 100000) if self.precision[0] < 1 else self.precision[0]
        # other_res = round(other.precision[0] * 100000) if other.precision[0] < 1 else other.precision[0]
        # is_same_box = str(self.box[0]) == str(other.box[0])
        # is_same_box &= str(self.box[1]) == str(other.box[1])
        # is_same_box &= str(self.box[2]) == str(other.box[2])
        # is_same_box &= str(self.box[3]) == str(other.box[3])
        return self.box and self.precision == other.precision

    @property
    def box(self):
        """
        经纬度范围
        [max_lat, max_lon, min_lat, min_lon]
        """
        return self.provider.box

    @property
    def precision(self):
        """
        分辨率
        """
        return self.provider.precision

    @property
    def imgsize(self):
        """图像尺寸 [w, h]"""
        return self.provider.imgsize
    @property
    def projection(self):
        """投影方式"""
        return self.provider.projection
