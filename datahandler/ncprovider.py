# coding=utf-8
"""
Create by fantasylin 2018/11/27
"""
from decimal import Decimal
import numpy as np
from netCDF4 import Dataset
from osgeo import osr
# from customerr.cus_error import BandError

class NCProvider(object):
    """
    NC文件读取类
    """
    def __init__(self, filename):
        self.nc_obj = Dataset(filename, "a", format="NETCDF4")
        self.attrs = self.nc_obj
        self.dataset = []
        # self.proj_info = self.nc_obj.variables['geospatial_lat_lon_extent']

    def get_dataset(self, dataset):
        """获取数据集
        :param dataset 数据集名
        """
        try:
            self.dataset = np.array(self.nc_obj.variables[dataset])
            if self.dataset.ndim == 3:
                self.dataset = self.dataset[1, :, :]
        except KeyError as key_err:
            raise BandError(key_err.message)
        else:
            return self.dataset
    def get_atrribute(self, group, dataset, atrname):
        """
        获取dataset的atrname属性，因为nc文件下只有variables(dataset)
        没有group一层
        ：group默认是'/'，但实际并没有使用
        """
        if group != None and group != '/':
            group = '/'
        return self.nc_obj.variables[dataset].getncattr(atrname)
    @property
    def box(self):
        """
        经纬度范围
        """
        max_lat = self.nc_obj['geospatial_lat_lon_extent'].geospatial_north_latitude
        max_lon = self.nc_obj['geospatial_lat_lon_extent'].geospatial_east_longitude
        min_lat = self.nc_obj['geospatial_lat_lon_extent'].geospatial_south_latitude
        min_lon = self.nc_obj['geospatial_lat_lon_extent'].geospatial_west_longitude
        return [Decimal(str(max_lat)), Decimal(str(max_lon)), Decimal(str(min_lat)), Decimal(str(min_lon))]

    @property
    def precision(self):
        """
        分辨率
        """
        lat_precision = self.nc_obj['lat'].resolution
        # lon_precision = self.proj_info['geospatial_lon_resolution']
        # lon_precision = self.nc_obj['lon'].resolution
        # lon_precision = Decimal(str(lon_precision))
        # return [lat_precision, lon_precision]
        lat_precision = Decimal(str(lat_precision))
        return lat_precision

    @property
    def imgsize(self):
        """图像尺寸"""
        height, width = self.dataset.shape
        return [width, height]

    @property
    def projection(self):
        """投影方式"""
        proj = self.attrs.Project
        if 'GLL' in proj:
            proj = 'Latlon'
        elif 'AEA' in proj:
            proj = 'Albers'
        srs = osr.SpatialReference()
        if proj != 'Lalon':
            srs.SetProjection(proj)
            srs.SetProjCS(proj)
        srs.SetWellKnownGeogCS('WGS84')
        return srs
if __name__ == '__main__':
    proj = 'Albers'
    srs = osr.SpatialReference()
    if proj != 'Lalon':
        srs.SetProjection(proj)
        srs.SetProjCS(proj)
    srs.SetWellKnownGeogCS('WGS84')