#!/usr/bin/python
#coding=utf-8
'''
    project=edu-test,
    name=image_normal.py,
    date=2019/12/27 0027,
    author='zhangxuya',
    author_email='lionhenryzxy@sina.com',
    function：
    calculate the change compare the past
    prerequisite:
        based on Python 3.x
        need Python module
    Any issues or improvements please contact author
'''
import os
import sys
from PIL import Image
if __name__ == '__main__':
    # path = r'D:\DATA\tile'#sys.argv[1]
    # for dir_root,dir_dirs,dir_files in os .walk(path):
    #     for single in dir_files:
    #         if '.JPG' in single:
    #             singlepath = os.path.join(dir_root,single)
    #             outpath = os.path.join(r'D:\DATA\newtile',single.split('_')[2][1:]+'.JPG')
    #             tempimg = Image.open(singlepath)
    #             tempimg = tempimg.resize((4096,4096),Image.ANTIALIAS)
    #             tempimg.save(outpath)
    #             print 'transform from {}--------->{}'.format(singlepath,outpath)
    singlepath = "D:\DATA\FY3D_MERSI_GBAL_L2_PAD_MLT_GLL_20191219_POAD_004KM_MS_03_02_01.JPG"
    outpath = os.path.join(r'D:\DATA\newtile','all.JPG')
    tempimg = Image.open(singlepath)
    tempimg = tempimg.resize((8192,4096),Image.ANTIALIAS)
    tempimg.save(outpath)
    print 'transform from {}--------->{}'.format(singlepath,outpath)