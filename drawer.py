#coding=utf-8
'''
drawer class
palette format support single,range,gradient
based on Python 2.x
need Python numpy,json module
Other
    Any issues or improvements please contact zhangxy
'''

import json
from PIL import Image
import numpy as np

class Drawer(object):
    '''
    绘图类
    '''
    def __init__(self, cbfile):
        '''
        cbfile: 传入调色板文件
        '''
        self.colorbarfile = json.loads(cbfile)
        self.slope = self.colorbarfile['slope']
        self.palettetype = self.colorbarfile['defaultPalette']
        self.gradient_cb = []
        self.range_cb = []
        self.single_cb = []
        if 'gradient' in self.colorbarfile:
            self.gradient_cb = self.colorbarfile['gradient']
        if 'range' in self.colorbarfile:
            self.range_cb = self.colorbarfile['range']
        if 'single' in self.colorbarfile:
            self.single_cb = self.colorbarfile['single']

    def getpixel(self, dataset):
        '''
        dataset: 传入矩阵数据
        '''
        tardataset = np.ones((dataset.shape[0], dataset.shape[1], 4))
        tardataset[::] = -1
        if 'single' in self.palettetype:
            tardataset = self.single_drawer(dataset, self.single_cb, tardataset)
            judgemask = tardataset == [-1, -1, -1, -1]
            if tardataset[judgemask].size == 0:
                return tardataset
        else:
            tardataset = self.single_drawer(dataset, self.single_cb, tardataset)
            judgemask = tardataset == [-1, -1, -1, -1]
            if tardataset[judgemask].size == 0:
                return tardataset
            elif 'gradient' in self.palettetype:
                tardataset = self.gradient_drawer(dataset, self.gradient_cb, tardataset)
                judgemask = tardataset == [-1, -1, -1, -1]
                if tardataset[judgemask].size == 0:
                    return tardataset
            elif 'range' in self.palettetype:
                tardataset = self.range_drawer(dataset, self.range_cb, tardataset)
                judgemask = tardataset == [-1, -1, -1, -1]
                if tardataset[judgemask].size == 0:
                    return tardataset
        raise Exception("value not in colorbar", dataset)

    def assigncolor(self, tardataset, mask, colorbar):
        if tardataset[mask].size > 0:
            if len(colorbar) >= 4:
                tardataset[mask] = colorbar
            else:
                tardataset[mask] =[colorbar[0], colorbar[1], colorbar[2], 255]
        return tardataset[mask]
    def single_drawer(self, dataset, colorbar, tardataset):
        '''
        根据single调色板绘图
        dataset: 数据矩阵
        colorbar： 调色板
        tardataset: 目标的的矩阵
        '''
        #特殊值的处理
        nullmask = np.isnan(dataset[:]) | np.isinf(dataset)
        tardataset[nullmask] = [255, 255, 255, 0]
        for index in range(0, len(colorbar)):
            #获取需要进行判断的值
            valuemask = tardataset[:,:] == [-1, -1 , -1, -1]
            #三维转二维，方便与dataset的mask合并
            valuemask = valuemask[:,:,0]
            mask = dataset == colorbar[index][0]
            tardataset[valuemask & mask] = self.assigncolor(tardataset, valuemask & mask, colorbar[index][1])
        return tardataset

    def range_drawer(self, dataset, colorbar, tardataset):
        '''
        根据range调色板绘图
        dataset: 数据矩阵
        colorbar： 调色板
        tardataset: 目标的的矩阵
        '''
        #特殊值的处理
        nullmask = np.isnan(dataset[:]) | np.isinf(dataset)
        tardataset[nullmask] = [255, 255, 255, 0]
        valuemask = tardataset[:,:] == [-1, -1 , -1, -1]
        valuemask = valuemask[:,:,0]
        mask = dataset <= colorbar[0][0][1]
        tardataset[valuemask & mask] = self.assigncolor(tardataset, valuemask & mask, colorbar[0][1])
        valuemask = tardataset[:,:] == [-1, -1 , -1, -1]
        valuemask = valuemask[:,:,0]
        mask = dataset > colorbar[-1][0][0]
        tardataset[valuemask & mask] = self.assigncolor(tardataset, valuemask & mask, colorbar[-1][1])
        for index in range(1, len(colorbar)-1):
            valuemask = tardataset[:,:] == [-1, -1 , -1, -1]
            valuemask = valuemask[:,:,0]
            mask = (dataset > colorbar[index][0][0]) & (dataset <= colorbar[index][0][1])
            tardataset[valuemask & mask] = self.assigncolor(tardataset, valuemask & mask, colorbar[index][1])
        return tardataset

    def gradient_drawer(self, dataset, colorbar, tardataset):
        '''
        根据gradient调色板绘图
        dataset: 数据矩阵
        colorbar： 调色板
        tardataset: 目标的的矩阵
        '''
        #特殊值的处理
        nullmask = np.isnan(dataset[:]) | np.isinf(dataset)
        tardataset[nullmask] = [255, 255, 255, 0]
        #小于最小值
        valuemask = tardataset[:,:] == [-1, -1 , -1, -1]
        valuemask = valuemask[:,:,0]
        mask = dataset <= colorbar[0][0]
        tardataset[valuemask & mask] = self.assigncolor(tardataset, valuemask & mask, colorbar[0][1])
        for index in range(0, len(colorbar)-1):
            #获取需要进行判断的值
            valuemask = tardataset[:,:] == [-1, -1 , -1, -1]
            if index == 18:
                print(valuemask.shape)
            valuemask = valuemask[:,:,0]
            mask = (dataset >colorbar[index][0]) & (dataset <= colorbar[index+1][0])
            tempmask = valuemask & mask
            if tempmask[tempmask == True].size > 0:
                    ratio = (1.0 * (dataset[valuemask & mask] - colorbar[index][0]) / (colorbar[index + 1][0] - colorbar[index][0])).reshape(-1,1)
                    colorrange =  (np.array(colorbar[index + 1][1] - np.array(colorbar[index][1]))).reshape(1,-1)
                    temp = np.dot(ratio, colorrange) + np.array(colorbar[index][1])
                    if len(colorbar[index][1]) < 4:
                        alphaband = np.ones((temp.shape[0], 1))
                        alphaband[::] = 255
                        temp = np.column_stack((temp, alphaband))
                    tardataset[valuemask & mask] = temp
        #大于最大值
        valuemask = tardataset[:,:] == [-1, -1 , -1, -1]
        valuemask = valuemask[:,:,0]
        mask = dataset > colorbar[-1][0]
        tardataset[valuemask & mask] = self.assigncolor(tardataset, valuemask & mask, colorbar[-1][1])
        return tardataset

    def drawjpg(self, dataset, outpath):
        '''
        JPG出图
        dataset: 输出数据
        outpath： 输出文件
        '''
        tardataset = self.getpixel(dataset * self.slope)
        new_image = Image.fromarray(tardataset[:,:,0:3].astype(np.uint8)).convert('RGB')
        new_image.save(outpath, 'jpeg')

    def drawpng(self, dataset, outpath):
        '''
        PNG出图
        dataset: 输出数据
        outpath： 输出文件
        '''
        tardataset = self.getpixel(dataset * self.slope)
        new_image = Image.fromarray(tardataset.astype(np.uint8)).convert('RGBA')
        new_image.save(outpath, 'png')
    def drawarray(self, dataset):
        '''
        PNG出图
        dataset: 输出数据
        outpath： 输出文件
        '''
        tardataset = self.getpixel(dataset)
        return tardataset.astype(np.uint8)
    def close(self):
        '''
        消除
        '''
        del self.colorbarfile
        del self.slope
        del self.palettetype
        del self.gradient_cb
        del self.range_cb
        del self.single_cb